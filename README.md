# Wireshark

[https://www.wireshark.org/docs/wsug_html/](https://www.wireshark.org/docs/wsug_html/)
[https://www.wireshark.org/#download](https://www.wireshark.org/#download)

## tcpdump 

- i ：监听的网卡 (eth0)
- port: 端口
- w: 写入文件名

```
tcpdump -vvv -i 网卡名 port 端口号 -w 结果写入的文件名.pcap
```

示例：

- 抓取包含端口22的数据包

```
tcpdum -vvv -i eth0 port 22
```


- 抓取包含10.10.10.0/24网段的数据包

```
tcpdum -vvv -i eth0 net 10.10.10.0/24
```

- 抓取包含10.10.10.122的数据包 

```
tcpdum -vvv -i eth0 host 10.10.10.122
```

## wireshark 使用

[https://www.freebuf.com/sectool/256745.html](https://www.freebuf.com/sectool/256745.html)

- Mac无法使用wireshark

```
sudo chmod 777 /dev/bpf*
```
